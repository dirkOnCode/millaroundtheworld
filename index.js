/* 
 Created on : 11.05.2017
 Author     : dirk
 */


// Setup basic express server
var express = require('express');
var app = express();


// now we need to parse body to receive commands and messages
var bodyParser = require('body-parser');
// var multer = require('multer'); 
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
// app.use(multer()); // for parsing multipart/form-data

var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;
// the env.USER is not set when start node server as service
var user = process.env.USER || "pi";

// RPi B+ and RPi2+3 and Zero
var gpioPortControlWatchStart  = process.env.GPIO_START || 5;   // PIN 29
var gpioPortControlWatchStop   = process.env.GPIO_STOP || 6;   // PIN 31

// am'I the Raspberry User?
var isRaspberryUser = (user === "pi" || user === "root");       

// queue controler
var EventEmitter;
var queueEventEmitter;

// init stopWatch
var Stopwatch = require('timer-stopwatch');
var stopwatch = new Stopwatch(); // A new count up stopwatch. Starts at 0console.log(stopwatch);

// Infinite loop to show the timer
var InfiniteLoop = null;

// message when server start to listen
server.listen(port, function() {
    console.log('user:' + user);
    console.log('Server listening at port %d', port);
    console.log("gpioPortStopWatch  = %d", gpioPortControlWatchStart);
    console.log("gpioPortStopWatch  = %d\n", gpioPortControlWatchStop);

    EventEmitter = require("events").EventEmitter;
    queueEventEmitter = new EventEmitter();

    /*
     * start timer
     */
    queueEventEmitter.on(watchCommands.start, function() {

        if( !stopwatch.state && stopwatch.ms == 0) {
            stopwatch.reset();
            stopwatch.start();
        }
    });

    /*
     * pause timer
     */
    queueEventEmitter.on(watchCommands.pause, function() {
    	stopwatch.startstop();
    });

    /*
     * stop timer
     */
    queueEventEmitter.on(watchCommands.stop, function() {
    	if( stopwatch.state && stopwatch.ms >= 4000) {
			stopwatch.stop();
			watchHistory.push(stopwatch.ms);
			emitWatchHistory();
		}
    });

    /*
     * reset timer
     */
    queueEventEmitter.on(watchCommands.reset, function() {
    	if( !stopwatch.state) {
			stopwatch.reset();
		}
    });

    /*
     * refresh history
     */
    queueEventEmitter.on(watchCommands.history, function() {
    	watchHistory = [];
    	emitWatchHistory();
    });

    // init queue status
    queueEventEmitter.emit(watchCommands.history, "history");

    function intervalFunc () {
	    // update watch display
	    io.emit(clientMessage.updateWatch, millisecAsString(stopwatch.ms));
        emitGpioStatus();
	}

	InfiniteLoop = setInterval(intervalFunc, 100);

});



// rest api for easy messaging from server to server
app.post('/rest/', function (req, res) {
    var source = req.body.source || "guest";
    res.sendStatus(202);
});

// Routing, show index.html in /public folder
app.use(express.static(__dirname + '/public'));


/*
 * stop watch handler
 * here starts the main handling of the blow-in events
 */

/*
 * watchHistory contains all the commands for doing the wind
 */
var watchHistory = [];

/*
 * historyQueueMaxLength limits the History
 */
var historyQueueMaxLength = 10;


/*
 * windCommands contains all valid admin queue commands
 */
var watchCommands = {
    start : 'start'
    ,pause:  'pause'
    ,stop:  'stop'
    ,reset:  'reset'
    ,history:  'history'
};

/*
 * clientMessage contains valid messages for the client 
 */
var clientMessage = {
    updateWatch : "update watch"
    ,readQueuedHistory : "readQueueHistory"
    ,commandEcho : "echo"
    ,updateGpioStatus : "update gpio status"
};


/*
 * GPIOMock is used when you test this code on another system than a raspberry
 */
function GPIOMock (port,mode) {
    var myPort = port;
    var activeLowInverted = false;
    this.writeSync = function(onoff) {
        console.log(timestampString() + myPort + " > mock writeSync " + onoff);
        return;
    };
    this.readSync = function() {
        console.log(timestampString() + myPort + " > mock readSync ");
        return 9;
    };
    this.watch = function(callback) {
        console.log(timestampString() + myPort + " > mock watch " + callback);
        return;
    };
    this.unexport = function() {
        console.log("\nmock unexport: ");
        return;
    };
    this.activeLow = function() {
    	return activeLowInverted;
    }
    this.setActiveLow = function(inverted) {
    	activeLowInverted = !activeLowInverted;
    }
}

/*
 * am'I runnuing on a Pi?
 */
if( isRaspberryUser ) {
    // yes, so inti gpio
    var GPIO = require('onoff').Gpio;
}
else {
    // no, so create an gpio mock, when not on Pi
    var GPIO = GPIOMock;
    console.log("not a RaspberryUser ? enable GPIO Mock ");
}

// set start and stop watch port to input
// 'none', 'falling', 'rising', or 'both
var gpioWatchStart = new GPIO(gpioPortControlWatchStart, 'in', 'both');
var gpioWatchStop = new GPIO(gpioPortControlWatchStop, 'in', 'rising');

gpioWatchStart.setActiveLow(true);
gpioWatchStop.setActiveLow(true);

console.log("gpios inverted : " + gpioWatchStart.activeLow() + " / " + gpioWatchStop.activeLow());


/*
 * socket io handler
 */
io.on('connection', function(socket) {

	emitWatchHistory();
    console.log("connected");

    // listen to controls
    socket.on('control', function(data) {
        switch(data.control) {
            case "start":
                queueEventEmitter.emit(watchCommands.start, "start");
                break;
            case "pause":
                queueEventEmitter.emit(watchCommands.pause, "pause");
                break;
            case "stop":
                queueEventEmitter.emit(watchCommands.stop, "stop");
                break;
            case "reset":
                queueEventEmitter.emit(watchCommands.reset, "reset");
                break;
            case "history":
                queueEventEmitter.emit(watchCommands.history, "history");
                break;
            default:
                console.log("unknown command:" + msg);
        }        
        io.emit(clientMessage.commandEcho, data.control + " - ok");
    });

	emitGpioStatus();

});

// GPIO handler

gpioWatchStart.watch(function (err, value) {
    setTimeout( function() {

        console.log("Start - " + getTime());
        if( gpioWatchStart.readSync() ) {
            queueEventEmitter.emit(watchCommands.start, "start");
        }
        else {
            console.log("Start aborted");
        }
    },200);
	emitGpioStatus();
});

gpioWatchStop.watch(function (err, value) {
    setTimeout( function() {
        console.log("Stop -" + getTime());
        if( gpioWatchStop.readSync() ) {
            queueEventEmitter.emit(watchCommands.stop, "stop");
        }
        else {
            console.log("Stop aborted");
        }
    },200);
	emitGpioStatus();
});



// Helper function

function getTime() {
    var d = new Date();
    var n = d.getTime();
    return n;
}

function emitGpioStatus()
{
    io.emit(clientMessage.updateGpioStatus, {start : gpioWatchStart.readSync(), stop : gpioWatchStop.readSync() });
}

function emitWatchHistory() {
	var watchHistoryOutput = [];

	watchHistory.sort(function(a, b){return a - b});
	watchHistory.forEach(function(value){
	  watchHistoryOutput.push(millisecAsString(value));
	});
    io.emit(clientMessage.readQueuedHistory, watchHistoryOutput );
}

function millisecAsString(millis) {
    var milliseconds = parseInt((millis%1000)/100)
        , seconds = parseInt((millis/1000)%60)
        , minutes = parseInt((millis/(1000*60))%60)
        , hours = parseInt((millis/(1000*60*60))%24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return /*hours + ":" + */ minutes + ":" + seconds + "." + milliseconds;

}

function timestampString()
{
    return "[" + new Date().toISOString() + "] ";
}


// programm termination

function exit() {
	gpioWatchStart.unwatchAll();
	gpioWatchStart.unexport();
	gpioWatchStop.unexport();
	gpioWatchStop.unwatchAll();

	if(InfiniteLoop) {clearInterval(InfiniteLoop);}
	process.exit();
}

process.on('SIGINT', exit);



/* 
 Created on : 11.05.2017
 Author     : dirk
 */

var socket = io();


socket.on('readQueueHistory', function(historyQueue) {
    $('#QueueHistory').empty();
    $(historyQueue).each(function(){
        $('#QueueHistory').append($('<li>').append(JSON.stringify(this)));
        $('#QueueHistoryBackground').append($('<li>').append(" 88:88.8 "));
    });
});

socket.on('echo', function(msg) {
    $('#commandEcho').prepend($('<li>').append(JSON.stringify(msg)));
});

socket.on('update watch', function(msg) {
    $('#watch').html(msg);
});

socket.on('update gpio status', function(data) {
    $('#status #start').html(data.start);
    $('#status #stop').html(data.stop);
});

$("a").click(function(event) {
	console.log($(this).attr("rel"));
    socket.emit("control", {control: $(this).attr("rel")});

});

$("#status").click( function(event) {
	$("#controls").toggle();
})


$("#controls").hide();

